# FACE RECOGNITION API  #
The following are steps to setup the server application on ubuntu based OS.

- Create 2 directories named _"image_dataset"_ and _"pickles"_ on the same level as the _"main.py"_ script.

- Install python3, please refer the [website](https://www.python.org/downloads/) to check on how to install python on to the machine. Preferred version (3 < version < 3.7).

- Install Virtual environment with the command: _pip install virtualenv_. You can check the [website](https://pypi.org/project/virtualenv/) for installing and creating a virtual environment and it's use on their website. Create a virtual environment with a name you'd prefer, using the command: _virtualenv name_of_env_

- Activate the virtual environment using the command: _source name_of_env/bin/activate_. You'd see (name_of_env) on the CLI when it's activated.

- Install the packages enlisted in requirements.txt as: _pip install -r requirements.txt_

Now the server application is all set.

To run the server, execute the command: _python main.py_