import os
import pickle
import cv2
import numpy as np
import imutils
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC, OneClassSVM


def pickle_embeddings(embeddings_path, data):
    pickle_data = {"embeddings":[], "names":[]}

    if os.path.exists(embeddings_path):
        pickle_data = pickle.loads(open(embeddings_path, "rb").read())

    #  append embeddings to existing pickle data or new data to be pickled
    for i in range(len(data["embeddings"])):
        
        #append extracted data to pickle data for pickling
        pickle_data["embeddings"].append(data["embeddings"][i])
        pickle_data["names"].append(data["names"][i])

    #write embeddings.pickle to disk
    f = open(embeddings_path, 'wb')
    f.write(pickle.dumps(pickle_data))
    f.close()
    
    return pickle_data

def extract_embeddings_for_image(image, protoPath, modelPath, embedding_model_path):
    #load the face detector and face embedding model from disk
    detector = cv2.dnn.readNetFromCaffe(protoPath, modelPath)
    embedder = cv2.dnn.readNetFromTorch(embedding_model_path)
    
    # image = imutils.resize(image, width=600)
    (h, w) = image.shape[:2]

    imageBlob = cv2.dnn.blobFromImage(
        cv2.resize(image, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0), swapRB=True)

    # apply OpenCV's deep learning-based face detector to localize faces in the input image
    detector.setInput(imageBlob)
    detections = detector.forward()

    # ensure at least one face was found
    if len(detections) > 0:
        i = np.argmax(detections[0, 0, :, 2])
        confidence = detections[0, 0, i, 2]

        # ensure that the detection with the 50% probabilty thus helping filter out weak detections
        if confidence > 0.5:
            # compute the actual coordinates of the bounding box for the face
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            # extract the face ROI and grab the ROI dimensions
            face = image[startY:endY, startX:endX]
            (fH, fW) = face.shape[:2]

            ''' construct a blob for the face ROI, then pass the blob
                through the face embedding model to obtain the 128-d
                quantification of the face '''
            
            faceBlob = cv2.dnn.blobFromImage(face, 1.0 / 255, (96, 96), (0, 0, 0), swapRB=True, crop=False)
            embedder.setInput(faceBlob)
            vec = embedder.forward()

            return vec.flatten()

def train_model_and_save(pickle_data, recognizer_path, le_path):
    #encode labels
    le = LabelEncoder()
    labels = le.fit_transform(pickle_data["names"])

    # train the model used to accept the 128-d embeddings of the face and then produce the actual face recognition
    print("Training the model usng SVM...")

    if len(np.unique(pickle_data["names"])) == 1:
        recognizer = OneClassSVM(kernel="rbf", gamma=0.005)
        recognizer.fit(pickle_data["embeddings"])
    
    else:        
        recognizer = SVC(C=1000.0, kernel="rbf", probability=True, class_weight='balanced', gamma=0.005)
        recognizer.fit(pickle_data["embeddings"], labels)

    # write the actual face recognition model to disk
    f = open(recognizer_path, "wb")
    f.write(pickle.dumps(recognizer))
    f.close()

    # write the label encoder to disk
    f = open(le_path, "wb")
    f.write(pickle.dumps(le))
    f.close()
    
    return True