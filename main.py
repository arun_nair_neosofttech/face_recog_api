import re, os, io, base64, uuid
from io import BytesIO
import pickle
import cv2
import numpy as np
from flask import Flask, request, Response
import json
from PIL import Image
from flask_wtf.csrf import CSRFProtect


from config import Config
from trainer import pickle_embeddings, train_model_and_save, extract_embeddings_for_image
app = Flask(__name__)
csrf = CSRFProtect(app)
app.config.from_object(Config)

dataset_path = app.config['DATASET_PATH']
detector_path = app.config['DETECTOR_PATH']
embedding_model_path = app.config['EMBEDDING_MODEL']
embeddings_path = app.config['EMBEDDINGS_PATH']
recognizer_path = app.config["RECOGNIZER_PATH"]
le_path = app.config["LE_PATH"]
protoPath = os.path.sep.join([detector_path, "deploy.prototxt"])
modelPath = os.path.sep.join([detector_path, "res10_300x300_ssd_iter_140000.caffemodel"])


html_string_for_index = '''
    <h2>You are not lost buddy!</h2>
    <p>Try the valid API endpoints.</p>
    <p><span style="font-weight:bold;font-style:italic;">/register/</span> - To register the user</p>
    <p><span style="font-weight:bold;font-style:italic;">/authenticate/</span> - To authenticate the user</p>
'''


@app.route('/', methods=['GET'])
@csrf.exempt
def index():
    return Response(html_string_for_index, status=200)


@app.route('/register', methods=['POST'])
@csrf.exempt
def register():
    knownEmbeddings = []
    knownNames = []

    email = request.json.get('email', None)
    images = request.json.get('images', None)

    valid_fields = validate_fields(email, images)

    if not valid_fields:
        response = app.response_class(
            response=json.dumps({"message":"All fields are required"}),
            status=400,
            mimetype='application/json'
            )        
        return response
    

    valid_email = validate_email(email.lower())

    if not valid_email:
        response = app.response_class(
            response=json.dumps({"message":"Invalid email id"}),
            status=400,
            mimetype='application/json'
            )
        return response

    IMAGE_PATH = dataset_path + email.split('@')[0].replace('.','_')
    if not os.path.exists(IMAGE_PATH):
        
        #creates new folder for storing user's images
        os.makedirs(IMAGE_PATH)
    
    # loops through all images submitted
    for i, image in enumerate(images): 
        imagePath = os.path.join(IMAGE_PATH, str(uuid.uuid4()) + '.jpg')
        data = image.split('data:image/jpeg;base64,')[1]

        if not len(data)%4==0:
            padding = 4 - len(data)%4
            data+= '='*padding
        
        img = base64.urlsafe_b64decode(data)

        #saves the binary file
        with open(imagePath, "wb") as f:
            f.write(img)
        
        image = cv2.imread(imagePath)
        embedding = extract_embeddings_for_image(image, protoPath, modelPath, embedding_model_path)
        
        knownEmbeddings.append(embedding)
        knownNames.append(email)

    data = {"embeddings": knownEmbeddings, "names": knownNames}

    #creates/updates pickle file and retrieves updated pickle data 
    pickle_data = pickle_embeddings(embeddings_path, data)
    
    #trains and creates a new face recognition model
    train_model_and_save(pickle_data, recognizer_path, le_path)
    
    data = {"success":True,"message": "User registered","user":email}
    
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )

    return response


@app.route('/authenticate', methods=['POST'])
@csrf.exempt
def authenticate():
    #generates name for storing the image on the disk temporarily
    imagePath = str(uuid.uuid4()) + '.jpg' 

    im_b64 = request.form.get('image', None)
    email = request.form.get('email', None)

    valid_fields = validate_fields(email, im_b64)

    if not valid_fields:
        response = app.response_class(
            response=json.dumps({"message":"All fields are required"}),
            status=400,
            mimetype='application/json'
            )        
        return response
    

    valid_email = validate_email(email.lower())

    if not valid_email:
        response = app.response_class(
            response=json.dumps({"message":"Invalid email id"}),
            status=400,
            mimetype='application/json'
            )
        return response

    data = im_b64.split('data:image/jpeg;base64,')[1]

    #rectify padding
    if not len(data)%4==0:
        padding = 4 - len(data)%4
        data += '=' * padding
    
    im_binary = base64.urlsafe_b64decode(data)

    with open(imagePath, "wb") as f:
        f.write(im_binary)
    
    image = cv2.imread(imagePath)
    
    #deletes image from disk after reading
    os.remove(imagePath)

    #returns error if recognizer not found
    if not os.path.exists(recognizer_path):
        return Response("No users registered", status=400)

    #loads the face detector from disk
    detector = cv2.dnn.readNetFromCaffe(protoPath, modelPath)

    # loads the face embedding model from disk
    print("Loading Openface imlementation of Facenet model...")
    embedder = cv2.dnn.readNetFromTorch(embedding_model_path)

    # loads the face recognition model along with the label encoder
    recognizer = pickle.loads(open(recognizer_path, "rb").read())
    le = pickle.loads(open(le_path, "rb").read())

    #gets embeddings for passed image
    embedding = extract_embeddings_for_image(image, protoPath, modelPath, embedding_model_path)
    
    matched_user = 'Invalid user'
    success = False

    #performs classification to predict user
    if recognizer.__str__() == 'OneClassSVM(gamma=0.005)':
        score = recognizer.score_samples(embedding.reshape(1,-1))
        offset = recognizer.offset_
        print("Offset difference:", abs(score[0]-offset[0]) )
        if abs(score[0]-offset[0]) <= 0.003:
            if le.classes_[0] == email:
                matched_user = le.classes_[0]
                success = True
    
    else:
        preds = recognizer.predict_proba(embedding.reshape(1,-1))[0]
        j = np.argmax(preds)
        if le.classes_[j] == email:
            matched_user = le.classes_[j]
            success = True
            
        print(preds)
        print(le.classes_)
    
    if not success:
        data = {"success":success,"message": matched_user}
    else:
        data = {"success":success,"user_matched": matched_user}

    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )

    return response


#validate email field
def validate_email(email):
    regex = r'^[a-z0-9\._]+[@]\w+[\w\.]+$'
    if not re.search(regex,email):  
        return False          
    return True

#validate fields
def validate_fields(email, image):
    if not(email and image):
        return False
    return True


if __name__ == "__main__":
    app.run(debug=True)