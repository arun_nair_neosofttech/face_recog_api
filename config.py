import os

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'cd48e1c22de0961d5d1bfb14f8a66e006cfb1cfbf3f0c0f3'
    UPLOAD_EXTENSIONS =  ['.jpg', '.jpeg']
    DATASET_PATH = 'image_dataset/'
    EMBEDDINGS_PATH = 'pickles/embeddings.pickle'
    DETECTOR_PATH = 'face_detection_model/'
    EMBEDDING_MODEL = 'nn4.small2.v1.t7'
    RECOGNIZER_PATH = 'pickles/recognizer.pickle'
    LE_PATH = 'pickles/le.pickle'
    FLASK_APP = 'main.py'
    FLASK_ENV = 'development'

